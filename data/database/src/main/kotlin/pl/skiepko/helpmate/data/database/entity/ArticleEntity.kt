package pl.skiepko.helpmate.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import pl.skiepko.helpmate.core.model.Article

@Entity(tableName = "articles")
data class ArticleEntity(
    @PrimaryKey
    val id: Int,
    val title: String,
    val link: String,
    val preview: String
) {
    constructor(article: Article) : this(
        id = article.id,
        title = article.title,
        link = article.link,
        preview = article.preview
    )
}

fun ArticleEntity.toDomain() = Article(
    id = id,
    title = title,
    link = link,
    preview = preview
)
