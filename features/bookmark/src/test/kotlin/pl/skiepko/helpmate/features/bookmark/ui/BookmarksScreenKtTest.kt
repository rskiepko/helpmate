package pl.skiepko.helpmate.features.bookmark.ui

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasScrollToNodeAction
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onFirst
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performScrollToIndex
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import pl.skiepko.helpmate.core.testing.data.articlesTestData
import pl.skiepko.helpmate.resources.R

@RunWith(RobolectricTestRunner::class)
class BookmarksScreenKtTest {
    private lateinit var noResultsString: String
    private lateinit var initialStateString: String
    private lateinit var emptyQueryString: String
    private lateinit var errorString: String

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Before
    fun setup() {
        composeTestRule.activity.apply {
            noResultsString = getString(R.string.empty_bookmarks_list)
            initialStateString = getString(R.string.txt_initial_state)
            emptyQueryString = getString(R.string.empty_query)
            errorString = getString(R.string.error_unknown)
        }
    }

    @Test
    fun `should show general error`() {
        composeTestRule.setContent {
            BookmarksScreen(
                uiState = BookmarksUiState.Error,
            )
        }

        composeTestRule
            .onNodeWithText(errorString)
            .assertIsDisplayed()
    }

    @Test
    fun `empty search result should show no results view`() {
        composeTestRule.setContent {
            BookmarksScreen(
                uiState = BookmarksUiState.Success(),
            )
        }

        composeTestRule
            .onNodeWithText(noResultsString)
            .assertIsDisplayed()
    }

    @Test
    fun `should show loader view`() {
        composeTestRule.setContent {
            BookmarksScreen(
                uiState = BookmarksUiState.Loading,
            )
        }

        composeTestRule
            .onNodeWithTag(LOADER_TAG)
            .assertIsDisplayed()
    }

    @Test
    fun `search results with all articles visible`() {
        composeTestRule.setContent {
            BookmarksScreen(
                uiState = BookmarksUiState.Success(
                    bookmarks = articlesTestData
                )
            )
        }

        val scrollableNode = composeTestRule
            .onAllNodes(hasScrollToNodeAction())
            .onFirst()

        articlesTestData.forEachIndexed { index, article ->
            scrollableNode.performScrollToIndex(index)

            composeTestRule
                .onNodeWithText(article.title)
                .assertIsDisplayed()
        }
    }

}
