plugins {
    alias(libs.plugins.helpmate.feature)
}

android {
    namespace = "pl.skiepko.helpmate.features.article"
}

dependencies {
    testImplementation(libs.robolectric)
}
