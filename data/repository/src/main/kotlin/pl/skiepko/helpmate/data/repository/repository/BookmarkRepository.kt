package pl.skiepko.helpmate.data.repository.repository

import kotlinx.coroutines.flow.Flow
import pl.skiepko.helpmate.core.model.Article

interface BookmarkRepository {
    fun getBookmarks(): Flow<List<Article>>
    fun isBookmarked(id: Int): Flow<Boolean>
    suspend fun save(article: Article)
    suspend fun remove(id: Int)
}
