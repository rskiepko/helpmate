package pl.skiepko.helpmate.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.skiepko.helpmate.data.database.dao.ArticleDao
import pl.skiepko.helpmate.data.database.entity.ArticleEntity

@Database(
    entities = [
        ArticleEntity::class
    ],
    version = 1,
    exportSchema = true
)
internal abstract class HelpmateDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
}
