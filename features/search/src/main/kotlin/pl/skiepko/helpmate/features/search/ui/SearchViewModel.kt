package pl.skiepko.helpmate.features.search.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import pl.skiepko.helpmate.core.logger.Logger
import pl.skiepko.helpmate.core.model.SearchResult
import pl.skiepko.helpmate.features.search.usecase.SearchArticlesUseCase
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    searchArticlesUseCase: SearchArticlesUseCase
) : ViewModel() {
    private val _page = MutableStateFlow(1)

    private val _loadingNextPage = MutableStateFlow(false)
    val loadingNextPage get() = _loadingNextPage.asStateFlow()

    private val _loading = MutableStateFlow(false)
    val loading get() = _loading.asStateFlow()

    private val _query = MutableStateFlow("")
    val query get() = _query.asStateFlow()

    val searchUiState: StateFlow<SearchUiState> = query
        .debounce(600)
        .combine(_page.asStateFlow()) { queryValue, pageValue -> Pair(queryValue, pageValue) }
        .distinctUntilChanged()
        .flatMapLatest { (queryValue, pageValue) ->
            if (queryValue.isBlank()) {
                flowOf(SearchUiState.EmptyQuery)
            } else {
                flowOf(searchArticlesUseCase(queryValue, pageValue))
                    .onStart { _loading.update { true } }
                    .map<SearchResult, SearchUiState> { response ->
                        SearchUiState.Success(
                            articles = response.articles,
                            canGetMoreData = !response.pagination.isLastPage
                        )
                    }
            }
        }
        .catch { exception ->
            Logger.e(exception) { "Error occurred while fetching getting search results" }
            emit(SearchUiState.Error)
        }
        .onEach { _loading.update { false } }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = SearchUiState.Initial,
        )

    fun onEvent(event: SearchEvent) {
        when (event) {
            is SearchEvent.QueryChanged -> updateQuery(event.value.trim())
            is SearchEvent.LoadNextPage -> fetchNextPage()
        }
    }

    private fun fetchNextPage() {
        if (loadingNextPage.value) return
        _loadingNextPage.update { true }
        _page.update { it + 1 }
    }

    private fun updateQuery(query: String) {
        _page.update { 1 }
        _query.update { query }
    }
}
