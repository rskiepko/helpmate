plugins {
    alias(libs.plugins.helpmate.library)
    alias(libs.plugins.helpmate.hilt)
}

android {
    namespace = "pl.skiepko.helpmate.core.testing"
}

dependencies {
    api(libs.kotlinx.coroutines.test)
    api(projects.core.model)
    api(projects.data.repository)

    implementation(libs.androidx.test.rules)
    implementation(libs.hilt.testing)
}
