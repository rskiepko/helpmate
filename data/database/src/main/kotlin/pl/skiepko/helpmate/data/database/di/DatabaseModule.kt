package pl.skiepko.helpmate.data.database.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import pl.skiepko.helpmate.data.database.HelpmateDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object DatabaseModule {

    @Provides
    @Singleton
    fun provideHelpmateDatabase(@ApplicationContext context: Context) = Room.databaseBuilder(
        context = context,
        klass = HelpmateDatabase::class.java,
        name = "helpmateDatabase"
    ).build()
}
