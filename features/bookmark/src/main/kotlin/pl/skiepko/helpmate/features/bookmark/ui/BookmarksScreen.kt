package pl.skiepko.helpmate.features.bookmark.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.core.ui.component.ArticleRow
import pl.skiepko.helpmate.core.ui.component.BackButton
import pl.skiepko.helpmate.core.ui.component.GeneralError
import pl.skiepko.helpmate.core.ui.component.HelpmateTopAppBar
import pl.skiepko.helpmate.core.ui.component.Loader
import pl.skiepko.helpmate.core.ui.theme.spacing
import pl.skiepko.helpmate.resources.R

internal const val LOADER_TAG = "LoaderTag"

@Composable
fun BookmarksRoute(
    navigateUp: () -> Unit,
    openArticle: (Article) -> Unit,
    viewModel: BookmarksViewModel = hiltViewModel()
) {
    val bookmarksUiState by viewModel.bookmarksUiState.collectAsStateWithLifecycle()

    BookmarksScreen(
        uiState = bookmarksUiState,
        navigateUp = navigateUp,
        openArticle = openArticle
    )
}

@Composable
internal fun BookmarksScreen(
    uiState: BookmarksUiState = BookmarksUiState.Loading,
    navigateUp: () -> Unit = {},
    openArticle: (Article) -> Unit = {}
) {
    Scaffold(
        topBar = {
            HelpmateTopAppBar(
                title = { Text(text = stringResource(id = R.string.bookmarks)) },
                navigationIcon = { BackButton(onClick = navigateUp) }
            )
        }
    ) { padding ->
        Box(modifier = Modifier.padding(padding)) {
            when (uiState) {
                BookmarksUiState.Error -> GeneralError(modifier = Modifier.fillMaxSize())
                BookmarksUiState.Loading -> Loader(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag(LOADER_TAG)
                )

                is BookmarksUiState.Success -> {
                    if (uiState.bookmarks.isEmpty()) {
                        NoResultsState()
                    } else {
                        SavedBookmarks(
                            bookmarks = uiState.bookmarks,
                            onBookmarkClick = openArticle
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun SavedBookmarks(
    bookmarks: List<Article>,
    onBookmarkClick: (Article) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.normal),
        modifier = modifier.fillMaxSize()
    ) {
        items(items = bookmarks, key = { it.id }) { item ->
            ArticleRow(
                title = item.title,
                preview = item.preview,
                onClick = { onBookmarkClick(item) },
                modifier = Modifier.padding(horizontal = MaterialTheme.spacing.normal)
            )
        }
    }
}

@Composable
private fun NoResultsState(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = stringResource(id = R.string.empty_bookmarks_list),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}
