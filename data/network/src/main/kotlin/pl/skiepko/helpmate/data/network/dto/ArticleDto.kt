package pl.skiepko.helpmate.data.network.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.json.JSONException
import org.json.JSONObject
import pl.skiepko.helpmate.core.logger.Logger
import pl.skiepko.helpmate.core.model.Article

@Serializable
data class ArticleDto(
    @SerialName("id")
    val id: Int?,
    @SerialName("title")
    val title: String?,
    @SerialName("link")
    val link: String?,
    @SerialName("preview")
    val jsonPreview: String?
)

fun ArticleDto.toDomain() = Article(
    id = id ?: -1,
    title = title.orEmpty(),
    link = "https://pomoc.autopay.pl$link",
    preview = try {
        val preview = JSONObject(jsonPreview.orEmpty())
        preview.getString("text")
    } catch (ex: JSONException) {
        Logger.e(ex) { "Error occurred while extracting text from jsonPreview" }
        ""
    }
)
