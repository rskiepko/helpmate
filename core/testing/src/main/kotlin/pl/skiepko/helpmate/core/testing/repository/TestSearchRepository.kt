package pl.skiepko.helpmate.core.testing.repository

import pl.skiepko.helpmate.core.model.Pagination
import pl.skiepko.helpmate.core.model.SearchResult
import pl.skiepko.helpmate.core.testing.data.articlesTestData
import pl.skiepko.helpmate.data.repository.repository.SearchRepository

class TestSearchRepository : SearchRepository {
    private var shouldThrowException = false

    private val emptyResponse = SearchResult(
        articles = emptyList(),
        pagination = Pagination(currentPage = 1, totalPages = 1)
    )

    private val data = articlesTestData

    override suspend fun searchArticles(query: String, page: Int): SearchResult {
        if (shouldThrowException)
            error("Test error")

        if (query.isEmpty())
            return emptyResponse

        val articles = data.filter { it.title.contains(query) }
            .drop(SearchRepository.PAGE_LIMIT * (page - 1))
            .take(SearchRepository.PAGE_LIMIT)

        return SearchResult(
            articles = articles,
            pagination = Pagination(
                currentPage = page,
                isLastPage = page * SearchRepository.PAGE_LIMIT >= data.count()
            )
        )
    }

    fun setShouldThrowException(value: Boolean) {
        shouldThrowException = value
    }
}
