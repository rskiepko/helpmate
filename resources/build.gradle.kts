plugins {
    alias(libs.plugins.helpmate.library)
}

android {
    namespace = "pl.skiepko.helpmate.resources"
}
