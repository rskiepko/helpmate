package pl.skiepko.helpmate.core.ui.theme

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Immutable
data class Spacing(
    val default: Dp = 0.dp,
    val two: Dp = 2.dp,
    val quarter: Dp = 4.dp,
    val half: Dp = 8.dp,
    val normal: Dp = 16.dp,
    val big: Dp = 24.dp,
    val huge: Dp = 32.dp,
    val extreme: Dp = 48.dp
)

val LocalSpacing = compositionLocalOf { Spacing() }

@Suppress("UnusedReceiverParameter")
val MaterialTheme.spacing: Spacing
    @Composable
    @ReadOnlyComposable
    get() = LocalSpacing.current
