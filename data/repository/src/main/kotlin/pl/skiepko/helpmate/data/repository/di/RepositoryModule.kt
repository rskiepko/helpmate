package pl.skiepko.helpmate.data.repository.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pl.skiepko.helpmate.data.repository.repository.BookmarkRepository
import pl.skiepko.helpmate.data.repository.repository.SearchRepository
import pl.skiepko.helpmate.data.repository.repository.impl.BookmarkRepositoryImpl
import pl.skiepko.helpmate.data.repository.repository.impl.SearchRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
internal interface RepositoryModule {

    @Binds
    fun bindsSearchRepository(repo: SearchRepositoryImpl): SearchRepository

    @Binds
    fun bindsArticleRepository(repo: BookmarkRepositoryImpl): BookmarkRepository
}
