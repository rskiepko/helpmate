package pl.skiepko.helpmate.features.article.ui

import androidx.lifecycle.SavedStateHandle
import androidx.navigation.testing.invoke
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import pl.skiepko.helpmate.core.navigation.HelpmateGraph
import pl.skiepko.helpmate.core.testing.data.firstTestArticle
import pl.skiepko.helpmate.core.testing.repository.TestBookmarkRepository
import pl.skiepko.helpmate.core.testing.util.MainDispatcherRule
import pl.skiepko.helpmate.features.article.usecase.IsArticleBookmarkedUseCase
import pl.skiepko.helpmate.features.article.usecase.ToggleArticleBookmarkUseCase
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class ArticleViewModelTest {
    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private val article = firstTestArticle

    private lateinit var repo: TestBookmarkRepository

    private lateinit var viewModel: ArticleViewModel

    @Before
    fun setup() {
        repo = TestBookmarkRepository()
        viewModel = ArticleViewModel(
            toggleArticleBookmarkUseCase = ToggleArticleBookmarkUseCase(repo),
            isArticleBookmarkedUseCase = IsArticleBookmarkedUseCase(repo),
            savedStateHandle = SavedStateHandle(
                route = HelpmateGraph.ArticleDetails(
                    id = article.id,
                    title = article.title,
                    link = article.link,
                    preview = article.preview
                )
            )
        )
    }

    @Test
    fun `bookmarked is initially false`() = runTest {
        // Then
        assertFalse { viewModel.isBookmarked.value }
    }

    @Test
    fun `should toggled article bookmark`() = runTest {
        // Given
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.isBookmarked.collect() }

        // When
        viewModel.toggleBookmark()

        // Then
        assertTrue { viewModel.isBookmarked.value }

        // When
        viewModel.toggleBookmark()

        // Then
        assertFalse { viewModel.isBookmarked.value }

        collectJob.cancel()
    }
}
