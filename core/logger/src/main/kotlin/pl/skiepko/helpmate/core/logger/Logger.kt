package pl.skiepko.helpmate.core.logger

import co.touchlab.kermit.Logger
import co.touchlab.kermit.crashlytics.CrashlyticsLogWriter

@Suppress("unused")
object Logger {
    init {
        if (!BuildConfig.DEBUG) {
            @Suppress("OPT_IN_USAGE")
            Logger.setLogWriters(CrashlyticsLogWriter())
        }
    }

    fun v(throwable: Throwable? = null, tag: String = Logger.tag, message: () -> String) {
        Logger.v(throwable, tag, message)
    }

    fun d(throwable: Throwable? = null, tag: String = Logger.tag, message: () -> String) {
        Logger.d(throwable, tag, message)
    }

    fun i(throwable: Throwable? = null, tag: String = Logger.tag, message: () -> String) {
        Logger.i(throwable, tag, message)
    }

    fun w(throwable: Throwable? = null, tag: String = Logger.tag, message: () -> String) {
        Logger.w(throwable, tag, message)
    }

    fun e(throwable: Throwable? = null, tag: String = Logger.tag, message: () -> String) {
        Logger.e(throwable, tag, message)
    }
}
