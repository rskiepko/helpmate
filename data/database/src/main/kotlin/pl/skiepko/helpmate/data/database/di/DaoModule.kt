package pl.skiepko.helpmate.data.database.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pl.skiepko.helpmate.data.database.HelpmateDatabase

@Module
@InstallIn(SingletonComponent::class)
internal object DaoModule {
    @Provides
    fun providesArticleDao(db: HelpmateDatabase) = db.articleDao()
}
