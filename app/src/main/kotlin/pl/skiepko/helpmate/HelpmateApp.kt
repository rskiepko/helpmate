package pl.skiepko.helpmate

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HelpmateApp : Application()
