package pl.skiepko.helpmate.features.article.ui

import android.annotation.SuppressLint
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Bookmark
import androidx.compose.material.icons.filled.BookmarkBorder
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.core.ui.component.BackButton
import pl.skiepko.helpmate.core.ui.component.HelpmateTopAppBar

@Composable
fun ArticleRoute(
    navigateUp: () -> Unit,
    viewModel: ArticleViewModel = hiltViewModel()
) {
    val isBookmarked by viewModel.isBookmarked.collectAsStateWithLifecycle()

    ArticleScreen(
        article = viewModel.article,
        isBookmarked = isBookmarked,
        toggleBookmark = viewModel::toggleBookmark,
        navigateUp = navigateUp
    )
}

@Composable
private fun ArticleScreen(
    article: Article,
    isBookmarked: Boolean,
    toggleBookmark: () -> Unit,
    navigateUp: () -> Unit
) {
    var showLoader by remember { mutableStateOf(true) }
    Scaffold(
        topBar = {
            HelpmateTopAppBar(
                title = { Text(text = article.title, maxLines = 1) },
                navigationIcon = { BackButton(onClick = navigateUp) },
                actions = {
                    BookmarkButton(
                        isBookmarked = isBookmarked,
                        onClick = toggleBookmark
                    )
                }
            )
        }
    ) { padding ->
        Column(modifier = Modifier.padding(padding)) {
            AnimatedVisibility(visible = showLoader) {
                LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
            }
            HelpmateWebView(
                url = article.link,
                onPageLoaded = { showLoader = false },
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
            )
        }
    }
}

@Composable
private fun BookmarkButton(
    isBookmarked: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(
        onClick = onClick,
        modifier = modifier
    ) {
        AnimatedContent(
            targetState = isBookmarked,
            label = "Saved icon animation"
        ) { saved ->
            if (saved) {
                Icon(
                    imageVector = Icons.Default.Bookmark,
                    contentDescription = null
                )
            } else {
                Icon(
                    imageVector = Icons.Default.BookmarkBorder,
                    contentDescription = null
                )
            }
        }
    }
}

@SuppressLint("SetJavaScriptEnabled")
@Composable
private fun HelpmateWebView(
    url: String,
    onPageLoaded: () -> Unit,
    modifier: Modifier = Modifier
) {
    AndroidView(
        modifier = modifier,
        factory = {
            WebView(it).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                settings.javaScriptEnabled = true
                settings.loadWithOverviewMode = true
                settings.useWideViewPort = true
                settings.setSupportZoom(true)
                webViewClient = object : WebViewClient() {
                    override fun onPageFinished(view: WebView?, url: String?) {
                        onPageLoaded()
                        super.onPageFinished(view, url)
                    }
                }
            }
        },
        update = { it.loadUrl(url) }
    )
}
