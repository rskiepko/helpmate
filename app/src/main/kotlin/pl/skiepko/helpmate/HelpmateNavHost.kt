package pl.skiepko.helpmate

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import pl.skiepko.helpmate.core.navigation.HelpmateGraph
import pl.skiepko.helpmate.features.article.ui.ArticleRoute
import pl.skiepko.helpmate.features.bookmark.ui.BookmarksRoute
import pl.skiepko.helpmate.features.search.ui.SearchRoute

@Composable
fun HelpmateNavHost(modifier: Modifier = Modifier) {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = HelpmateGraph.Search,
        modifier = modifier
    ) {
        composable<HelpmateGraph.Search> {
            SearchRoute(
                openArticle = {
                    navController.navigate(
                        HelpmateGraph.ArticleDetails(
                            id = it.id,
                            title = it.title,
                            link = it.link,
                            preview = it.preview
                        )
                    )
                },
                openBookmarks = {
                    navController.navigate(HelpmateGraph.Bookmarks)
                }
            )
        }
        composable<HelpmateGraph.ArticleDetails> {
            ArticleRoute(
                navigateUp = navController::navigateUp
            )
        }
        composable<HelpmateGraph.Bookmarks> {
            BookmarksRoute(
                navigateUp = navController::navigateUp,
                openArticle = {
                    navController.navigate(
                        HelpmateGraph.ArticleDetails(
                            id = it.id,
                            title = it.title,
                            link = it.link,
                            preview = it.preview
                        )
                    )
                }
            )
        }
    }
}
