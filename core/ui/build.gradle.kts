plugins {
    alias(libs.plugins.helpmate.library)
    alias(libs.plugins.helpmate.library.compose)
}

android {
    namespace = "pl.skiepko.helpmate.core.ui"
}

dependencies {
    implementation(projects.core.model)
    implementation(projects.core.logger)
    implementation(projects.resources)

    api(libs.androidx.compose.foundation)
    api(libs.androidx.compose.foundation.layout)
    api(libs.androidx.compose.material.iconsExtended)
    api(libs.androidx.compose.material3)
    api(libs.androidx.compose.runtime)
    api(libs.androidx.compose.ui.tooling.preview)
    api(libs.androidx.compose.ui.tooling)
    api(libs.androidx.compose.ui.util)
}
