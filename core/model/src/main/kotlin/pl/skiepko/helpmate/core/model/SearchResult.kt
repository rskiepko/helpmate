package pl.skiepko.helpmate.core.model

data class SearchResult(
    val articles: List<Article>,
    val pagination: Pagination
)
