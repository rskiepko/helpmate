package pl.skiepko.helpmate.core.model

data class Pagination(
    val currentPage: Int,
    val isLastPage: Boolean
) {
    constructor(currentPage: Int, totalPages: Int) : this(
        currentPage = currentPage,
        isLastPage = currentPage == totalPages
    )
}
