package pl.skiepko.helpmate.data.network.interfaces

import pl.skiepko.helpmate.data.network.dto.SearchDto
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {
    @GET("/search-engine/search")
    suspend fun searchArticles(
        @Query("query") query: String,
        @Query("per-page") perPage: Int,
        @Query("page") page: Int,
    ): SearchDto
}