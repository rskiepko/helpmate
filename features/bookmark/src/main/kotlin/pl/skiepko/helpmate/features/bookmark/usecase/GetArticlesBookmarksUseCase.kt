package pl.skiepko.helpmate.features.bookmark.usecase

import pl.skiepko.helpmate.data.repository.repository.BookmarkRepository
import javax.inject.Inject

class GetArticlesBookmarksUseCase @Inject constructor(
    private val repo: BookmarkRepository
) {
    operator fun invoke() = repo.getBookmarks()
}
