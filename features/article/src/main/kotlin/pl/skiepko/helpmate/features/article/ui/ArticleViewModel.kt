package pl.skiepko.helpmate.features.article.ui

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.toRoute
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.core.navigation.HelpmateGraph
import pl.skiepko.helpmate.features.article.usecase.IsArticleBookmarkedUseCase
import pl.skiepko.helpmate.features.article.usecase.ToggleArticleBookmarkUseCase
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel @Inject constructor(
    private val toggleArticleBookmarkUseCase: ToggleArticleBookmarkUseCase,
    isArticleBookmarkedUseCase: IsArticleBookmarkedUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    val article = savedStateHandle.toRoute<HelpmateGraph.ArticleDetails>().let {
        Article(
            id = it.id,
            title = it.title,
            link = it.link,
            preview = it.preview
        )
    }

    val isBookmarked: StateFlow<Boolean> = isArticleBookmarkedUseCase(article.id)
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = false,
        )

    fun toggleBookmark() {
        viewModelScope.launch {
            toggleArticleBookmarkUseCase.invoke(
                article = article,
                isBookmarked = isBookmarked.value
            )
        }
    }
}
