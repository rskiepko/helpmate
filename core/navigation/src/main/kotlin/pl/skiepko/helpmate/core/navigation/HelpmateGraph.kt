package pl.skiepko.helpmate.core.navigation

import kotlinx.serialization.Serializable

@Serializable
sealed class HelpmateGraph {
    @Serializable
    data object Search : HelpmateGraph()

    @Serializable
    data object Bookmarks : HelpmateGraph()

    @Serializable
    data class ArticleDetails(
        val id: Int,
        val title: String,
        val link: String,
        val preview: String
    ) : HelpmateGraph()
}
