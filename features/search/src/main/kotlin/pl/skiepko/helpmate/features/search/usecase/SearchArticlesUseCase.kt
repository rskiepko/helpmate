package pl.skiepko.helpmate.features.search.usecase

import pl.skiepko.helpmate.data.repository.repository.SearchRepository
import javax.inject.Inject

class SearchArticlesUseCase @Inject constructor(
    private val repo: SearchRepository
) {
    suspend operator fun invoke(query: String, page: Int) = repo.searchArticles(query, page)
}
