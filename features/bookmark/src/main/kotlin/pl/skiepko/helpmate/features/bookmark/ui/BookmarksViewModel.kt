package pl.skiepko.helpmate.features.bookmark.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import pl.skiepko.helpmate.core.logger.Logger
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.features.bookmark.usecase.GetArticlesBookmarksUseCase
import javax.inject.Inject

@HiltViewModel
class BookmarksViewModel @Inject constructor(
    getArticlesBookmarksUseCase: GetArticlesBookmarksUseCase
) : ViewModel() {

    val bookmarksUiState: StateFlow<BookmarksUiState> = getArticlesBookmarksUseCase()
        .map<List<Article>, BookmarksUiState> { bookmarks -> BookmarksUiState.Success(bookmarks) }
        .catch { exception ->
            Logger.e(exception) { "Error occurred while fetching saved bookmarks" }
            emit(BookmarksUiState.Error)
        }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = BookmarksUiState.Loading,
        )
}
