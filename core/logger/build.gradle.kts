plugins {
    alias(libs.plugins.helpmate.library)
}

android {
    namespace = "pl.skiepko.helpmate.core.logger"
}

dependencies {
    implementation(libs.kermit)
    implementation(libs.kermit.crashlytics)
}
