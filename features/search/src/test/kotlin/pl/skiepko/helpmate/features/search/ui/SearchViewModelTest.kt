package pl.skiepko.helpmate.features.search.ui

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import pl.skiepko.helpmate.core.testing.repository.TestSearchRepository
import pl.skiepko.helpmate.core.testing.util.MainDispatcherRule
import pl.skiepko.helpmate.features.search.usecase.SearchArticlesUseCase
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertTrue
import kotlin.time.Duration.Companion.seconds

class SearchViewModelTest {
    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private lateinit var searchRepository: TestSearchRepository

    private lateinit var viewModel: SearchViewModel

    @Before
    fun setup() {
        searchRepository = TestSearchRepository()

        viewModel = SearchViewModel(
            searchArticlesUseCase = SearchArticlesUseCase(repo = searchRepository)
        )
    }

    @Test
    fun `state is initially Initial`() = runTest {
        // Then
        assertEquals(SearchUiState.Initial, viewModel.searchUiState.value)
    }

    @Test
    fun `state is empty query when search query is empty`() = runTest {
        // Given
        val query = ""
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.searchUiState.collect() }

        // When
        viewModel.onEvent(SearchEvent.QueryChanged(query))
        advanceTimeBy(1.seconds)

        // Then
        assertEquals(SearchUiState.EmptyQuery, viewModel.searchUiState.value)

        collectJob.cancel()
    }

    @Test
    fun `state is no results with not matching query`() = runTest {
        // Given
        val query = "123123"
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.searchUiState.collect() }

        // When
        viewModel.onEvent(SearchEvent.QueryChanged(query))
        advanceTimeBy(1.seconds)

        // Then
        val result = viewModel.searchUiState.value
        assertIs<SearchUiState.Success>(result)
        assertTrue { result.articles.isEmpty() }

        collectJob.cancel()
    }

    @Test
    fun `state is success with matching query`() = runTest {
        // Given
        val query = "Kotlin"
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.searchUiState.collect() }

        // When
        viewModel.onEvent(SearchEvent.QueryChanged(query))
        advanceTimeBy(1.seconds)
        val result = viewModel.searchUiState.value

        // Then
        assertIs<SearchUiState.Success>(result)
        assertTrue { result.articles.isNotEmpty() }

        collectJob.cancel()
    }

    @Test
    fun `state is error when error occurred`() = runTest {
        // Given
        searchRepository.setShouldThrowException(true)
        val query = "Kotlin"
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.searchUiState.collect() }

        // When
        viewModel.onEvent(SearchEvent.QueryChanged(query))
        advanceTimeBy(1.seconds)
        val result = viewModel.searchUiState.value

        // Then
        assertIs<SearchUiState.Error>(result)

        collectJob.cancel()
    }
}
