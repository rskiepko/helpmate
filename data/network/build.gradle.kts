plugins {
    alias(libs.plugins.helpmate.library)
    alias(libs.plugins.helpmate.hilt)
}

android {
    namespace = "pl.skiepko.helpmate.data.network"
}

dependencies {
    implementation(projects.core.model)
    implementation(projects.core.logger)

    implementation(libs.network.retrofit)
    implementation(libs.network.retrofit.serialization)
    implementation(libs.network.okhttp3)
}
