package pl.skiepko.helpmate.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import kotlinx.coroutines.flow.Flow
import pl.skiepko.helpmate.data.database.entity.ArticleEntity

@Dao
interface ArticleDao {
    @Upsert
    suspend fun save(article: ArticleEntity)

    @Query("delete from articles where id = :id")
    suspend fun delete(id: Int)

    @Query("select exists(select * from articles where id = :id)")
    fun isExists(id: Int): Flow<Boolean>

    @Query("select * from articles")
    fun getArticles(): Flow<List<ArticleEntity>>
}
