package pl.skiepko.helpmate.data.network.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.skiepko.helpmate.data.network.BuildConfig
import pl.skiepko.helpmate.data.network.interfaces.SearchApi
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.kotlinx.serialization.asConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun okHttpCallFactory(): Call.Factory = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor()
                .apply {
                    if (BuildConfig.DEBUG) {
                        setLevel(HttpLoggingInterceptor.Level.BODY)
                    }
                }
        )
        .build()

    @Suppress("JSON_FORMAT_REDUNDANT")
    @Provides
    @Singleton
    fun jsonConverter(): Converter.Factory = Json {
        ignoreUnknownKeys = true
    }.asConverterFactory("application/json; charset=UTF8".toMediaType())

    @Provides
    @Singleton
    fun provideRetrofit(
        okhttpCallFactory: dagger.Lazy<Call.Factory>,
        converter: Converter.Factory
    ): Retrofit = Retrofit.Builder()
        .baseUrl("https://pomoc.bluemedia.pl")
        .addConverterFactory(converter)
        .callFactory { okhttpCallFactory.get().newCall(it) }
        .build()

    @Provides
    @Singleton
    fun provideHelpApi(retrofit: Retrofit): SearchApi = retrofit.create(SearchApi::class.java)
}
