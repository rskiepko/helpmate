plugins {
    alias(libs.plugins.helpmate.application)
    alias(libs.plugins.helpmate.application.compose)
    alias(libs.plugins.helpmate.application.firebase)
    alias(libs.plugins.helpmate.hilt)
}

android {
    namespace = "pl.skiepko.helpmate"

    defaultConfig {
        applicationId = "pl.skiepko.helpmate"
        versionCode = 1
        versionName = "1.0.0"

        testInstrumentationRunner = "pl.skiepko.helpmate.core.testing.HelpmateTestRunner"
        vectorDrawables.useSupportLibrary = true
    }

    buildTypes {
        release {
            isDebuggable = false
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    packaging {
        resources {
            excludes.add("/META-INF/{AL2.0,LGPL2.1}")
        }
    }
    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}

dependencies {
    implementation(projects.features.article)
    implementation(projects.features.bookmark)
    implementation(projects.features.search)

    implementation(projects.resources)
    implementation(projects.core.ui)
    implementation(projects.core.logger)
    implementation(projects.core.navigation)
    implementation(projects.core.model)

    implementation(libs.androidx.activity.compose)
    implementation(libs.androidx.hilt.navigation.compose)
    implementation(libs.androidx.navigation.compose)

    debugImplementation(libs.leakcanary)
}
