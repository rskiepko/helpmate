package pl.skiepko.helpmate.core.model.exception

import java.io.IOException

class NetworkUnavailableException : IOException()
