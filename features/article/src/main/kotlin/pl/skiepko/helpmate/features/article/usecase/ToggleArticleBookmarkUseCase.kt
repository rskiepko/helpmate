package pl.skiepko.helpmate.features.article.usecase

import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.data.repository.repository.BookmarkRepository
import javax.inject.Inject

class ToggleArticleBookmarkUseCase @Inject constructor(
    private val repo: BookmarkRepository
) {
    suspend operator fun invoke(article: Article, isBookmarked: Boolean) {
        if (isBookmarked) {
            repo.remove(article.id)
        } else {
            repo.save(article)
        }
    }
}
