package pl.skiepko.helpmate.features.search.ui

import pl.skiepko.helpmate.core.model.Article

sealed interface SearchUiState {
    data object Initial : SearchUiState
    data class Success(
        val articles: List<Article> = emptyList(),
        val canGetMoreData: Boolean = false
    ) : SearchUiState

    data object EmptyQuery : SearchUiState
    data object Error : SearchUiState
}
