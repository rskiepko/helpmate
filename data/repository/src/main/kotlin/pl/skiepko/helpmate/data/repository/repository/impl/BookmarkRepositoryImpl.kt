package pl.skiepko.helpmate.data.repository.repository.impl

import kotlinx.coroutines.flow.map
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.data.database.dao.ArticleDao
import pl.skiepko.helpmate.data.database.entity.ArticleEntity
import pl.skiepko.helpmate.data.database.entity.toDomain
import pl.skiepko.helpmate.data.repository.repository.BookmarkRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class BookmarkRepositoryImpl @Inject constructor(
    private val articleDao: ArticleDao
) : BookmarkRepository {
    override fun getBookmarks() = articleDao.getArticles()
        .map { list -> list.map { it.toDomain() } }

    override fun isBookmarked(id: Int) = articleDao.isExists(id)

    override suspend fun save(article: Article) {
        articleDao.save(ArticleEntity(article))
    }

    override suspend fun remove(id: Int) {
        articleDao.delete(id)
    }
}
