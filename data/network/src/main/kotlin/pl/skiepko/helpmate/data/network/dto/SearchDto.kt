package pl.skiepko.helpmate.data.network.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import pl.skiepko.helpmate.core.model.Pagination
import pl.skiepko.helpmate.core.model.SearchResult

@Serializable
data class SearchDto(
    @SerialName("items")
    val articles: List<ArticleDto>?,
    @SerialName("_meta")
    val metadata: MetadataDto?
) {
    @Serializable
    data class MetadataDto(
        @SerialName("pageCount")
        val pageCount: Int?,
        @SerialName("currentPage")
        val currentPage: Int?
    )
}

fun SearchDto.MetadataDto.toDomain() = Pagination(
    currentPage = currentPage ?: 0,
    totalPages = pageCount ?: 0
)

fun SearchDto.toDomain() = SearchResult(
    articles = articles?.map { it.toDomain() }.orEmpty(),
    pagination = metadata?.toDomain() ?: Pagination(currentPage = 0, totalPages = 0)
)
