package pl.skiepko.helpmate.data.repository.repository.impl

import pl.skiepko.helpmate.data.network.dto.toDomain
import pl.skiepko.helpmate.data.network.interfaces.SearchApi
import pl.skiepko.helpmate.data.repository.repository.SearchRepository
import javax.inject.Inject

internal class SearchRepositoryImpl @Inject constructor(
    private val api: SearchApi
) : SearchRepository {
    override suspend fun searchArticles(query: String, page: Int) =
        api.searchArticles(query = query, perPage = SearchRepository.PAGE_LIMIT, page = page).toDomain()
}
