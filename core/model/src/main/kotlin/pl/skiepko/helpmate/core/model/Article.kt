package pl.skiepko.helpmate.core.model

data class Article(
    val id: Int,
    val title: String,
    val link: String,
    val preview: String
)
