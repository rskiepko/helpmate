plugins {
    alias(libs.plugins.helpmate.library)
    alias(libs.plugins.helpmate.hilt)
    alias(libs.plugins.helpmate.room)
}

android {
    namespace = "pl.skiepko.helpmate.data.database"
}

dependencies {
    implementation(projects.core.model)
    implementation(projects.core.logger)
}
