package pl.skiepko.helpmate.features.bookmark.ui

import pl.skiepko.helpmate.core.model.Article

sealed interface BookmarksUiState {
    data object Loading : BookmarksUiState
    data object Error : BookmarksUiState
    data class Success(val bookmarks: List<Article> = emptyList()) : BookmarksUiState
}
