package pl.skiepko.helpmate.features.search.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Bookmarks
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import kotlinx.coroutines.delay
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.core.ui.component.ArticleRow
import pl.skiepko.helpmate.core.ui.component.GeneralError
import pl.skiepko.helpmate.core.ui.component.HelpmateTopAppBar
import pl.skiepko.helpmate.core.ui.theme.HelpmateTheme
import pl.skiepko.helpmate.core.ui.theme.spacing
import pl.skiepko.helpmate.resources.R

internal const val SEARCH_TEXT_FIELD_TAG = "SearchTextFieldTag"

@Composable
fun SearchRoute(
    openArticle: (Article) -> Unit,
    openBookmarks: () -> Unit,
    viewModel: SearchViewModel = hiltViewModel()
) {
    val query by viewModel.query.collectAsStateWithLifecycle()
    val loading by viewModel.loading.collectAsStateWithLifecycle()
    val articlesUiState by viewModel.searchUiState.collectAsStateWithLifecycle()
    val loadingNextPage by viewModel.loadingNextPage.collectAsStateWithLifecycle()

    SearchScreen(
        searchUiState = articlesUiState,
        query = query,
        loading = loading,
        loadingNextPage = loadingNextPage,
        onEvent = viewModel::onEvent,
        openArticle = openArticle,
        openBookmarks = openBookmarks
    )
}

@Composable
internal fun SearchScreen(
    searchUiState: SearchUiState = SearchUiState.Initial,
    query: String = "",
    loading: Boolean = true,
    loadingNextPage: Boolean = false,
    onEvent: (SearchEvent) -> Unit = {},
    openArticle: (Article) -> Unit = {},
    openBookmarks: () -> Unit = {}
) {
    val focusRequester = remember { FocusRequester() }
    Scaffold(
        topBar = {
            HelpmateTopAppBar(
                title = { Text(text = stringResource(id = R.string.app_name)) },
                actions = {
                    IconButton(onClick = openBookmarks) {
                        Icon(
                            imageVector = Icons.Default.Bookmarks,
                            contentDescription = null
                        )
                    }
                }
            )
        },
        modifier = Modifier.fillMaxSize()
    ) { padding ->
        Column(
            verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.quarter),
            modifier = Modifier
                .fillMaxSize()
                .padding(padding)
        ) {
            AnimatedVisibility(visible = loading) {
                LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
            }
            Column(
                verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.normal),
                modifier = Modifier.padding(horizontal = MaterialTheme.spacing.normal)
            ) {
                OutlinedTextField(
                    value = query,
                    onValueChange = { onEvent(SearchEvent.QueryChanged(it)) },
                    label = { Text(text = stringResource(id = R.string.search_hint)) },
                    modifier = Modifier
                        .fillMaxWidth()
                        .focusRequester(focusRequester)
                        .testTag(SEARCH_TEXT_FIELD_TAG)
                )

                when (searchUiState) {
                    SearchUiState.Initial -> InitialSearchState()
                    SearchUiState.Error -> GeneralError(modifier = Modifier.fillMaxSize())
                    SearchUiState.EmptyQuery -> EmptyQueryState()
                    is SearchUiState.Success -> {
                        if (searchUiState.articles.isEmpty()) {
                            NoResultsState()
                        } else {
                            ResultsState(
                                results = searchUiState.articles,
                                getNextPage = { onEvent(SearchEvent.LoadNextPage) },
                                openArticle = openArticle,
                                canGetMoreData = searchUiState.canGetMoreData,
                                loadingNextPage = loadingNextPage
                            )
                        }
                    }

                }
            }
        }
    }
    LaunchedEffect(Unit) {
        delay(500)
        focusRequester.requestFocus()
    }
}

@Composable
private fun InitialSearchState(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = stringResource(id = R.string.txt_initial_state),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun NoResultsState(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = stringResource(id = R.string.no_matching_articles_found),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun EmptyQueryState(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = stringResource(id = R.string.empty_query),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun ResultsState(
    results: List<Article>,
    canGetMoreData: Boolean,
    loadingNextPage: Boolean,
    getNextPage: () -> Unit,
    openArticle: (Article) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.normal),
        modifier = modifier.fillMaxSize()
    ) {
        items(
            items = results,
            key = { it.id },
            contentType = { "Articles search list items" }
        ) { item ->
            ArticleRow(
                title = item.title,
                preview = item.preview,
                onClick = { openArticle(item) }
            )
        }

        if (loadingNextPage) {
            item {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = MaterialTheme.spacing.big)
                ) {
                    CircularProgressIndicator()
                }
            }
        }
        if (canGetMoreData && !loadingNextPage) {
            item {
                LaunchedEffect(Unit) {
                    getNextPage()
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun SearchScreenPreview() {
    HelpmateTheme {
        SearchScreen(
            onEvent = {},
            openArticle = {},
            openBookmarks = {},
            searchUiState = SearchUiState.Initial,
            query = "",
            loadingNextPage = false,
            loading = true
        )
    }
}
