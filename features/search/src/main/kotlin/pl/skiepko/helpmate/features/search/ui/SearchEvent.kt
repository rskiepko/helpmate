package pl.skiepko.helpmate.features.search.ui

sealed class SearchEvent {
    data object LoadNextPage : SearchEvent()
    data class QueryChanged(val value: String) : SearchEvent()
}
