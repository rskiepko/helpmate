rootProject.name = "helpmate"

pluginManagement {
    includeBuild("build-logic")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode = RepositoriesMode.FAIL_ON_PROJECT_REPOS
    repositories {
        google()
        mavenCentral()
    }
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(":app")
include(":resources")
include(":core:logger")
include(":core:model")
include(":core:navigation")
include(":core:ui")
include(":core:testing")
include(":data:database")
include(":data:network")
include(":data:repository")
include(":features:article")
include(":features:search")
include(":features:bookmark")
