package pl.skiepko.helpmate.core.testing.data

import pl.skiepko.helpmate.core.model.Article

val firstTestArticle = Article(
    id = 1,
    title = "Introduction to Kotlin",
    link = "https://example.com/kotlin-intro",
    preview = "A beginner's guide to Kotlin programming language."
)
val secondTestArticle = Article(
    id = 2,
    title = "Advanced Kotlin Techniques",
    link = "https://example.com/kotlin-advanced",
    preview = "Master advanced Kotlin programming patterns."
)
val thirdTestArticle = Article(
    id = 3,
    title = "Kotlin Coroutines in Practice",
    link = "https://example.com/kotlin-coroutines",
    preview = "Learn how to use Kotlin Coroutines for asynchronous programming."
)

val articlesTestData = listOf(
    firstTestArticle,
    secondTestArticle,
    thirdTestArticle,
    Article(
        id = 4,
        title = "Understanding Kotlin's Null Safety",
        link = "https://example.com/kotlin-null-safety",
        preview = "An in-depth look at Kotlin's approach to null safety."
    ),
    Article(
        id = 5,
        title = "Kotlin vs Java: A Comparison",
        link = "https://example.com/kotlin-vs-java",
        preview = "A comparison between Kotlin and Java languages."
    ),
    Article(
        id = 6,
        title = "Functional Programming in Kotlin",
        link = "https://example.com/kotlin-functional",
        preview = "Explore functional programming concepts in Kotlin."
    ),
    Article(
        id = 7,
        title = "Kotlin for Android Development",
        link = "https://example.com/kotlin-android",
        preview = "How to develop Android applications using Kotlin."
    ),
    Article(
        id = 8,
        title = "Kotlin DSL: Domain-Specific Languages",
        link = "https://example.com/kotlin-dsl",
        preview = "Creating DSLs in Kotlin for more expressive code."
    ),
    Article(
        id = 9,
        title = "Unit Testing in Kotlin",
        link = "https://example.com/kotlin-testing",
        preview = "Best practices for writing unit tests in Kotlin."
    ),
    Article(
        id = 10,
        title = "Kotlin Multiplatform Projects",
        link = "https://example.com/kotlin-multiplatform",
        preview = "How to use Kotlin in cross-platform development."
    ),
    Article(
        id = 11,
        title = "Getting Started with Kotlin Native",
        link = "https://example.com/kotlin-native",
        preview = "Introduction to Kotlin Native for non-JVM targets."
    ),
    Article(
        id = 12,
        title = "Exploring Kotlin's Extension Functions",
        link = "https://example.com/kotlin-extensions",
        preview = "Using Kotlin's powerful extension functions."
    ),
    Article(
        id = 13,
        title = "Kotlin Collections: A Complete Guide",
        link = "https://example.com/kotlin-collections",
        preview = "An overview of Kotlin's collection framework."
    ),
    Article(
        id = 14,
        title = "Error Handling in Kotlin",
        link = "https://example.com/kotlin-error-handling",
        preview = "Strategies for handling errors in Kotlin applications."
    ),
    Article(
        id = 15,
        title = "Sealed Classes in Kotlin",
        link = "https://example.com/kotlin-sealed-classes",
        preview = "Using sealed classes for better control over class hierarchies."
    ),
    Article(
        id = 16,
        title = "Kotlin and Dependency Injection",
        link = "https://example.com/kotlin-di",
        preview = "Implementing dependency injection in Kotlin projects."
    ),
    Article(
        id = 17,
        title = "Best Practices for Kotlin Code",
        link = "https://example.com/kotlin-best-practices",
        preview = "A guide to writing clean and efficient Kotlin code."
    ),
    Article(
        id = 18,
        title = "Kotlin Lambda Expressions",
        link = "https://example.com/kotlin-lambdas",
        preview = "A look into Kotlin's lambda expressions and their use cases."
    ),
    Article(
        id = 19,
        title = "Kotlin Annotations Explained",
        link = "https://example.com/kotlin-annotations",
        preview = "How to use annotations effectively in Kotlin."
    ),
    Article(
        id = 20,
        title = "Exploring Kotlin's Companion Objects",
        link = "https://example.com/kotlin-companion-objects",
        preview = "Using companion objects in Kotlin classes."
    )
)
