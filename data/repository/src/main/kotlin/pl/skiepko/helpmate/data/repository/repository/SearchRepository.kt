package pl.skiepko.helpmate.data.repository.repository

import pl.skiepko.helpmate.core.model.SearchResult

interface SearchRepository {
    suspend fun searchArticles(query: String, page: Int): SearchResult

    companion object {
        const val PAGE_LIMIT = 20
    }
}
