package pl.skiepko.helpmate.core.testing.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import pl.skiepko.helpmate.core.model.Article
import pl.skiepko.helpmate.data.repository.repository.BookmarkRepository

class TestBookmarkRepository : BookmarkRepository {
    private val bookmarks = MutableStateFlow<List<Article>>(emptyList())
    private val shouldThrowException = MutableStateFlow(false)

    override fun getBookmarks(): Flow<List<Article>> = combine(shouldThrowException, bookmarks) { shouldThrow, bookmarks ->
        if (shouldThrow) {
            error("Test error")
        } else {
            bookmarks
        }
    }

    override fun isBookmarked(id: Int): Flow<Boolean> = getBookmarks()
        .map { it.any { article -> article.id == id } }

    override suspend fun save(article: Article) {
        bookmarks.update { list -> list.plus(article) }
    }

    override suspend fun remove(id: Int) {
        bookmarks.update { list -> list.find { it.id == id }?.let { list.minus(it) } ?: list }
    }

    fun setShouldThrowException(value: Boolean) {
        shouldThrowException.update { value }
    }

    fun setBookmarks(list: List<Article>) {
        bookmarks.update { list }
    }
}
