package pl.skiepko.helpmate.features.bookmark.ui

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import pl.skiepko.helpmate.core.testing.data.articlesTestData
import pl.skiepko.helpmate.core.testing.repository.TestBookmarkRepository
import pl.skiepko.helpmate.core.testing.util.MainDispatcherRule
import pl.skiepko.helpmate.features.bookmark.usecase.GetArticlesBookmarksUseCase
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertTrue
import kotlin.time.Duration.Companion.seconds

class BookmarksViewModelTest {
    @get:Rule
    val dispatcherRule = MainDispatcherRule()

    private lateinit var repo: TestBookmarkRepository

    private lateinit var viewModel: BookmarksViewModel

    @Before
    fun setup() {
        repo = TestBookmarkRepository()
        viewModel = BookmarksViewModel(
            getArticlesBookmarksUseCase = GetArticlesBookmarksUseCase(repo)
        )
    }

    @Test
    fun `state is initially Loading`() = runTest {
        // Then
        assertEquals(BookmarksUiState.Loading, viewModel.bookmarksUiState.value)
    }

    @Test
    fun `state is success with saved bookmarks`() = runTest {
        // Given
        repo.setBookmarks(articlesTestData)
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.bookmarksUiState.collect() }

        // When
        advanceTimeBy(1.seconds)
        val result = viewModel.bookmarksUiState.value

        // Then
        assertIs<BookmarksUiState.Success>(result)
        assertTrue { result.bookmarks.isNotEmpty() }

        collectJob.cancel()
    }

    @Test
    fun `state is success without saved bookmarks`() = runTest {
        // Given
        repo.setBookmarks(emptyList())
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.bookmarksUiState.collect() }

        // When
        advanceTimeBy(1.seconds)
        val result = viewModel.bookmarksUiState.value

        // Then
        assertIs<BookmarksUiState.Success>(result)
        assertTrue { result.bookmarks.isEmpty() }

        collectJob.cancel()
    }

    @Test
    fun `state is error when error occurred`() = runTest {
        // Given
        repo.setShouldThrowException(true)
        val collectJob = launch(dispatcherRule.dispatcher) { viewModel.bookmarksUiState.collect() }

        // When
        advanceTimeBy(1.seconds)
        val result = viewModel.bookmarksUiState.value

        // Then
        assertIs<BookmarksUiState.Error>(result)
        collectJob.cancel()
    }
}
