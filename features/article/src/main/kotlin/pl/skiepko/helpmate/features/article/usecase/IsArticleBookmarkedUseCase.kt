package pl.skiepko.helpmate.features.article.usecase

import pl.skiepko.helpmate.data.repository.repository.BookmarkRepository
import javax.inject.Inject

class IsArticleBookmarkedUseCase @Inject constructor(
    private val repo: BookmarkRepository
) {
    operator fun invoke(id: Int) = repo.isBookmarked(id)
}
