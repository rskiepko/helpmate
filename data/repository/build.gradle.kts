plugins {
    alias(libs.plugins.helpmate.library)
    alias(libs.plugins.helpmate.hilt)
}

android {
    namespace = "pl.skiepko.helpmate.data.repository"
}

dependencies {
    implementation(projects.core.model)
    implementation(projects.data.network)
    implementation(projects.data.database)
    implementation(projects.core.logger)
}
